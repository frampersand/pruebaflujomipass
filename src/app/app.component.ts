import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ModalController } from 'ionic-angular';

import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';
import { LoginPage } from '../pages/login/login';
import { CreatePassPage } from '../pages/create-pass/create-pass';
// import { SmsConfirmPage } from '../pages/sms-confirm/sms-confirm';
// import { CreatePassPage } from '../pages/create-pass/create-pass';
// import { PicturePromptPage } from '../pages/picture-prompt/picture-prompt';
import { MenuController } from 'ionic-angular';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  /* Oficial */
  rootPage:any = LoginPage;

  /* Pruebas */
  // rootPage:any = CreatePassPage;
  // rootPage:any = HomePage;

  @ViewChild(Nav) nav;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public modalCtrl: ModalController, public menuCtrl: MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  changePage(){
    console.log("CODIGO ABIERTO");

    //7 this.navCtrl.push(SettingsPage);
    this.menuCtrl.close();
    this.nav.push(SettingsPage);


    // const modal = this.modalCtrl.create(SettingsPage);
    // modal.present();
  }
  
}

