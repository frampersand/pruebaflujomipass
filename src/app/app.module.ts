import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PhoneConfirmPage } from "../pages/phone-confirm/phone-confirm";
import { SmsConfirmPage } from "../pages/sms-confirm/sms-confirm";
import { CreatePassPage } from "../pages/create-pass/create-pass";
import { SecurityDevicePage } from "../pages/security-device/security-device";
import { PicturePromptPage } from "../pages/picture-prompt/picture-prompt";
import { PictureConfirmPage } from "../pages/picture-confirm/picture-confirm";
import { SettingsPage } from "../pages/settings/settings";

import { PageConfirmarFotografiaPage } from '../pages/page-confirmar-fotografia/page-confirmar-fotografia';
import { PageAutorizarTomaDeFoto2Page } from '../pages/page-autorizar-toma-de-foto2/page-autorizar-toma-de-foto2';
import { PageAutenticarMiPassPage } from '../pages/page-autenticar-mi-pass/page-autenticar-mi-pass';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PhoneConfirmPage,
    LoginPage,
    SmsConfirmPage,
    CreatePassPage,
    SecurityDevicePage,
    PicturePromptPage,
    PictureConfirmPage,
    SettingsPage,
    PageConfirmarFotografiaPage,
    PageAutorizarTomaDeFoto2Page,
    PageAutenticarMiPassPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ''
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    PhoneConfirmPage,
    SmsConfirmPage,
    CreatePassPage,
    SecurityDevicePage,
    PicturePromptPage,
    PictureConfirmPage,
    SettingsPage,
    PageConfirmarFotografiaPage,
    PageAutorizarTomaDeFoto2Page,
    PageAutenticarMiPassPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}

