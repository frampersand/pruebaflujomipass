import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CachedResourceLoader } from '@angular/platform-browser-dynamic/src/resource_loader/resource_loader_cache';
import { AlertController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public fromSettings: boolean;
  public alert;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams ) {
    this.fromSettings = navParams.get('fromSettings');
    console.log(this.fromSettings);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PicturePromptPage');
    this.showAlert();
  }

  showAlert() {
    if(this.fromSettings == true){
      console.log("True");
      this.alert = this.alertCtrl.create({
        title: '¡Felicitaciones!',
        subTitle: 'Has registrado tu rostro satisfactoriamente y podrás usarlo para autorizar transacciones y firmar documentos',
        buttons: ['Cerrar']
      });
    }else{
      console.log("False");
      this.alert = this.alertCtrl.create({
        title: '¡Felicitaciones!',
        subTitle: 'Ya estás registrado en Mi Pass, con la cual podrás autorizar transacciones y firmar documentos',
        buttons: ['Cerrar']
      });
    }
    this.alert.present();
  }

  changePage() {
    this.navCtrl.push(SettingsPage);
  }

  
}
