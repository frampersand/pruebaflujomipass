import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PageAutorizarTomaDeFoto2Page } from './page-autorizar-toma-de-foto2';

@NgModule({
  declarations: [
    PageAutorizarTomaDeFoto2Page,
  ],
  imports: [
    IonicPageModule.forChild(PageAutorizarTomaDeFoto2Page),
  ],
})
export class PageAutorizarTomaDeFoto2PageModule {}
