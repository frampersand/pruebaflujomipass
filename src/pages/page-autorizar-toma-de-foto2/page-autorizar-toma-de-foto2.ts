import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { PageConfirmarFotografiaPage } from '../page-confirmar-fotografia/page-confirmar-fotografia';
import { SecurityDevicePage } from '../security-device/security-device';
import { PageAutenticarMiPassPage } from '../page-autenticar-mi-pass/page-autenticar-mi-pass';

/**
 * Generated class for the PageAutorizarTomaDeFoto2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-page-autorizar-toma-de-foto2',
  templateUrl: 'page-autorizar-toma-de-foto2.html',
})
export class PageAutorizarTomaDeFoto2Page {

  public picture: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PageAutorizarTomaDeFoto2Page');
  }

  takePicture(){

    this.navCtrl.push(PageAutenticarMiPassPage);
    /* console.log("click");
    this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        cameraDirection: this.camera.Direction.FRONT
    }).then((imageData) => {
        this.picture = "data:image/jpeg;base64," + imageData;

        this.navCtrl.push(PageConfirmarFotografiaPage, {
          picture: this.picture,
          // fromSettings: false
        });
        
    }, (err) => {
        console.log(err);
    }); */
  }

}
