import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PicturePromptPage } from '../picture-prompt/picture-prompt';

/**
 * Generated class for the SecurityDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-security-device',
  templateUrl: 'security-device.html',
})
export class SecurityDevicePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SecurityDevicePage');
  }

  changePage() {
    this.navCtrl.push(PicturePromptPage);
  }
}
