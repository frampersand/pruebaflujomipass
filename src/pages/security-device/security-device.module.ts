import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecurityDevicePage } from './security-device';

@NgModule({
  declarations: [
    SecurityDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(SecurityDevicePage),
  ],
})
export class SecurityDevicePageModule {}
