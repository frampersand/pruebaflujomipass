import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PictureConfirmPage } from './picture-confirm';

@NgModule({
  declarations: [
    PictureConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(PictureConfirmPage),
  ],
})
export class PictureConfirmPageModule {}
