import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the PictureConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-picture-confirm',
  templateUrl: 'picture-confirm.html',
})
export class PictureConfirmPage {
  public picture: string;
  public fromSettings: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.picture = navParams.get('picture');
    this.fromSettings = navParams.get('fromSettings');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PictureConfirmPage');
  }
  
  changePage() {
    let fromSettings = this.fromSettings;
    this.navCtrl.setRoot(HomePage, {
      fromSettings: fromSettings
    });
  }
}
