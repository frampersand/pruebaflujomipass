import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PageAutenticarMiPassPage } from './page-autenticar-mi-pass';

@NgModule({
  declarations: [
    PageAutenticarMiPassPage,
  ],
  imports: [
    IonicPageModule.forChild(PageAutenticarMiPassPage),
  ],
})
export class PageAutenticarMiPassPageModule {}
