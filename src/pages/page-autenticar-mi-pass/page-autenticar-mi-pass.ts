import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { PageConfirmarFotografiaPage } from '../page-confirmar-fotografia/page-confirmar-fotografia';

/**
 * Generated class for the PageAutenticarMiPassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-page-autenticar-mi-pass',
  templateUrl: 'page-autenticar-mi-pass.html',
})
export class PageAutenticarMiPassPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PageAutenticarMiPassPage');
  }

  public picture: string;


  takePicture(){

    // this.navCtrl.push(PageAutenticarMiPassPage);
    
    console.log("click");
    this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        cameraDirection: this.camera.Direction.FRONT
    }).then((imageData) => {
        this.picture = "data:image/jpeg;base64," + imageData;

        this.navCtrl.push(PageConfirmarFotografiaPage, {
          picture: this.picture,
          fromSettings: true
        });
        
    }, (err) => {
        console.log(err);
    });
  }


}
