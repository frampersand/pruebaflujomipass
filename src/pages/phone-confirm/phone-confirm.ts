import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SmsConfirmPage } from '../sms-confirm/sms-confirm'
/**
 * Generated class for the PhoneConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-phone-confirm',
  templateUrl: 'phone-confirm.html',
})
export class PhoneConfirmPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhoneConfirmPage');
  }
  
  changePage() {
    this.navCtrl.setRoot(SmsConfirmPage);
  }
}
