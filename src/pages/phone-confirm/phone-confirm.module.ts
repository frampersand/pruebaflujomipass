import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhoneConfirmPage } from './phone-confirm';

@NgModule({
  declarations: [
    PhoneConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(PhoneConfirmPage),
  ],
})
export class PhoneConfirmPageModule {}
