import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PageConfirmarFotografiaPage } from './page-confirmar-fotografia';

@NgModule({
  declarations: [
    PageConfirmarFotografiaPage,
  ],
  imports: [
    IonicPageModule.forChild(PageConfirmarFotografiaPage),
  ],
})
export class PageConfirmarFotografiaPageModule {}
