import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CreatePassPage } from '../create-pass/create-pass';
import { HomePage } from '../home/home';

/**
 * Generated class for the PageConfirmarFotografiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-page-confirmar-fotografia',
  templateUrl: 'page-confirmar-fotografia.html',
})
export class PageConfirmarFotografiaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.picture = navParams.get('picture');
    this.fromSettings = navParams.get('fromSettings') ? true : false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PageConfirmarFotografiaPage');
  }

  public picture: string;
  public fromSettings: Boolean;

  repetirFoto() {
    // this.navCtrl.push(Page3FotoCedulaAnversoPage);
    this.navCtrl.pop();
  }

  changePage() {
    if(this.fromSettings) {
      this.navCtrl.push(HomePage);
    } else {
      this.navCtrl.push(CreatePassPage);
    }
    
  }

}
