import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhoneConfirmPage } from '../phone-confirm/phone-confirm'

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  changePage() {
    this.navCtrl.push(PhoneConfirmPage);
  }

}
