import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SecurityDevicePage } from '../security-device/security-device';
import { HomePage } from '../home/home';

/**
 * Generated class for the CreatePassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-pass',
  templateUrl: 'create-pass.html',
})
export class CreatePassPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatePassPage');
  }

  changePage() {
    this.navCtrl.setRoot(HomePage);


    // this.navCtrl.push(HomePage);
  }
}
