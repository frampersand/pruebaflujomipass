import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatePassPage } from './create-pass';

@NgModule({
  declarations: [
    CreatePassPage,
  ],
  imports: [
    IonicPageModule.forChild(CreatePassPage),
  ],
})
export class CreatePassPageModule {}
