import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PicturePromptPage } from './picture-prompt';

@NgModule({
  declarations: [
    PicturePromptPage,
  ],
  imports: [
    IonicPageModule.forChild(PicturePromptPage),
  ],
})
export class PicturePromptPageModule {}
