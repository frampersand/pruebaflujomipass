import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { PictureConfirmPage } from '../picture-confirm/picture-confirm';
import { PageConfirmarFotografiaPage } from '../page-confirmar-fotografia/page-confirmar-fotografia';


/**
 * Generated class for the PicturePromptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-picture-prompt',
  templateUrl: 'picture-prompt.html',
})
export class PicturePromptPage {
  public base64Image: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PicturePromptPage');
  }
  takePicture(){
    console.log("click");
    this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        cameraDirection: this.camera.Direction.FRONT
    }).then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;

        this.navCtrl.push(PageConfirmarFotografiaPage, {
          picture: this.base64Image,
          // fromSettings: false
        });
        
    }, (err) => {
        console.log(err);
    });
  }

}
