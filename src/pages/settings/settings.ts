import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { PictureConfirmPage } from '../picture-confirm/picture-confirm';
import { PageAutorizarTomaDeFoto2Page } from '../page-autorizar-toma-de-foto2/page-autorizar-toma-de-foto2';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  public base64Image: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  closeModal() {
      this.navCtrl.pop();
  }
  takePicture(){

    this.navCtrl.push(PageAutorizarTomaDeFoto2Page);

    /* this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        cameraDirection: 1
    }).then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.navCtrl.push(PictureConfirmPage, {
          picture: this.base64Image,
          fromSettings: true
        });
        
    }, (err) => {
        console.log(err);
    }); */
  }
}
